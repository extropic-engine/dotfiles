
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General
"

" No VI classic
set nocompatible

" Disable modeline support for security
set nomodeline

" set up our cache directories
function! s:cache_path(suffix)
  return $XDG_CACHE_HOME . '/vim/' . a:suffix
endfunction

function! s:ensure_dir(dir)
  if !isdirectory(a:dir) | call mkdir(a:dir, "p") | endif
endfunction

let &directory  = s:cache_path('swap')
let &backupdir  = s:cache_path('backup')
let &undodir    = s:cache_path('undo')
let &viminfo   .= ',n' . s:cache_path('viminfo')

call s:ensure_dir(&directory)
call s:ensure_dir(&backupdir)
call s:ensure_dir(&undodir)

set encoding=utf-8

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"
" Enable these with :PlugInstall
"

call plug#begin('~/.vim/plugins')

Plug 'airblade/vim-gitgutter'
Plug 'editorconfig/editorconfig-vim'
Plug 'octref/RootIgnore'
Plug 'vim-scripts/restore_view.vim'
Plug 'mbbill/undotree'

" Language-specific plugins
Plug 'leafgarland/typescript-vim'  , { 'for' : 'typescript'     }
Plug 'rust-lang/rust.vim'
Plug 'tpope/vim-fugitive'
Plug 'dag/vim-fish'
Plug 'fatih/vim-go'
let g:go_version_warning = 0

" File browsing
Plug 'scrooloose/nerdtree'
" Quit Vim if nerdtree is the only buffer open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Make nerdtree behave more like the built in file browser
let NERDTreeHijackNetrw=1
let NERDTreeRespectWildIgnore=1

" Pretty statusline
Plug 'itchyny/lightline.vim'
let g:lightline = { 'colorscheme': 'seoul256' }

" Automatic syntax checking
" Cool, but too slow.
" Plug 'scrooloose/syntastic'
" let g:syntastic_enable_signs = 1
" let g:syntastic_check_on_open = 1
" let g:syntastic_check_on_wq = 0

" Better JSON handling
Plug 'elzr/vim-json'               , { 'for' : 'json'           }
let g:vim_json_syntax_conceal = 0 " Quote hiding is too annoying

" Fuzzy File Finding. After installing, you need to compile:
" https://github.com/wincent/command-t/blob/master/doc/command-t.txt
Plug 'wincent/command-t'

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors

"set background=dark
colorscheme fairyfloss

" TODO: only set the below if the term supports 256 colors
set t_Co=256
"let &colorcolumn="72,".join(range(80,999),",")
let &colorcolumn=join(range(1,80),",")
highlight ColorColumn ctermbg=232

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Indentation
"

" Use per-filetype indentation rules
filetype plugin indent on

" Type spaces when I hit tab
set expandtab

" Set tabs to 2 spaces
set tabstop=2
set softtabstop=2
set shiftwidth=2

" Not sure what these do
set showtabline=2
set bs=2

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" UI
"

" Syntax highlighting on
syntax enable

" Show relative line numbers
set rnu
set nu

" visual mode selection is exclusive (not sure what this does)
set selection=exclusive

" Show invisibles
set listchars=tab:»·,trail:•
set list

" Highlight whitespace when it shows up where we don't want it
highlight ExtraWhitespace ctermbg=green guibg=green
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhiteSpace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd Syntax * syn match Extrawhitespace /\s\+$\| \+\ze\t/

" Highlight matching parens
set showmatch

" Always display statusline
set laststatus=2

" Show info about the currently running command
set showcmd

" Show matches for command completion
set wildmenu

" show the completion menu even for just one match
set completeopt=menu,menuone,preview

" Show where the cursor is in the file
set ruler

" Enable mouse support
" (too annoying, it interfered with terminal mouse support)
" set mouse=a

" Fold based on language syntax
set foldmethod=syntax

" Folds open by default
set foldlevelstart=99

" restore_view plugin told me to do this
set viewoptions=cursor,folds,slash,unix

" Always show signs gutter
autocmd BufEnter * sign define dummy
autocmd BufEnter * execute 'sign place 9999 line=1 name=dummy buffer=' . bufnr('')

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Searching
"

" Make searches case insensitive
set ignorecase

" Unless they're mixed case
set smartcase

" Search while typing
set incsearch

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Filesystem
"

" automatically move working directory to the current file
set autochdir

" Handle classic Mac files in addition to DOS and UNIX line endings
set fileformats=unix,dos,mac

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Keymappings
"

" |      to split vertically
map \| :vsplit<cr>
" _      to split horizontally
map _ :split<cr>
" Ctrl+n to toggle file browser
nnoremap <C-n> :NERDTreeToggle<CR>

" faster movement between windows
map <C-j> <C-W>j " Ctrl+
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Get rid of Ex mode
nnoremap Q <Nop>

" Fuzzy File Finding
nnoremap <F3> :CommandT<CR>

nnoremap <f4> :checktime<CR>

map <f5> :make<cr>

" Quickly toggle folds
inoremap <F9> <C-O>za
nnoremap <F9> za
onoremap <F9> <C-C>za
vnoremap <F9> zf
