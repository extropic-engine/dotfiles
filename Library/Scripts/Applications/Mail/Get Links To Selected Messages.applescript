--
--	Created by: Nova
--	Created on: 5/4/21
--
-- TODO: make this work with Spark if possible
-- TODO: make this work with Messages.app if possible
--

use AppleScript version "2.4" -- Yosemite (10.10) or later
use scripting additions
use framework "Foundation"

-- classes, constants, and enums used
property NSURL : a reference to current application's NSURL
property NSString : a reference to the current application's NSString

tell application "Mail"
	set _textResult to ""
	set _listResult to {}
	set _messages to selection
	set _message_count to the count of _messages
	repeat with _i from 1 to _message_count
		set _messageID to content of header "message-id" of item _i of _messages
		set _title to subject of item _i of _messages
		set _rawURL to (NSString's stringWithString:_messageID)
		set _encodedURL to "message://" & (_rawURL's stringByAddingPercentEscapesUsingEncoding:4) as rich text -- 4 is NSUTF8StringEncoding
		set _textResult to _textResult & "[" & _title & "](" & _encodedURL & ")"
		set _resultItem to {title:_title, URL:_encodedURL}
		copy _resultItem to the end of _listResult
	end repeat
	set the clipboard to _textResult
	display notification "Copied " & _message_count & " items to clipboard!"
	return _listResult
end tell
