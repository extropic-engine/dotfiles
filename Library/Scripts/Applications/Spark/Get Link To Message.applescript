--
--	Created by: Nova
--	Created on: 5/4/21
--
-- TODO: this is currently wip

use AppleScript version "2.4" -- Yosemite (10.10) or later
use framework "Foundation"
use scripting additions

property NSURLComponents : a reference to the current application's NSURLComponents
property NSString : a reference to the current application's NSString

tell application "Spark"
	if HasSelectedMessage then
		set _messageTitle to GetSelectedMessageTitle
		set _sparkLink to GetSelectedMessageBacklink

		-- extract fields from backlink
		-- TODO: split this out to a separate function
		set _nsLink to (NSString's stringWithString:_sparkLink)
		set _blob to (_nsLink's stringByReplacingOccurrencesOfString:"readdle-spark://bl=" withString:"")
		set _urlDecodedBlobs to (_blob's stringByRemovingPercentEncoding)
		set _fields to (do shell script "echo '" & _urlDecodedBlobs & "' | base64 -d")
		set _nsComponentsArray to ((NSString's stringWithString:_fields)'s componentsSeparatedByString:";") as list
		-- TODO: extract message ID from second item

		-- TODO: write a function that takes an account id, message id, and third field (?) and creates a backlink

		display notification "This script is incomplete!"
	else
		display notification "One and only one message must be selected!"
	end if
end tell
