# Folder Descriptions

## `Application Scripts`

Applescript files placed here in specifically named subfolders will be made
available to the application with that bundle identifier. For example, scripts
placed in the `com.apple.mail` folder will be available to Mail.app for use by
Mail Rules.

## `Scripts`

Applescript files placed here will show up in the [Script Menu](https://developer.apple.com/library/archive/documentation/LanguagesUtilities/Conceptual/MacAutomationScriptingGuide/UsetheSystem-WideScriptMenu.html#//apple_ref/doc/uid/TP40016239-CH7-SW1) at all times.

## `Scripts/Applications`

Applescript files placed here in specifically named subfolders will show up in
the script menu when an app with that name is in the foreground. For example,
scripts placed in the "Safari" folder will show up when Safari is in the
foreground.

