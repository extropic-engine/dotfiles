--
--	Created by: Nova
--	Created on: 5/11/21
--
-- Detect magic links and automatically open them in Safari

use AppleScript version "2.4" -- Yosemite (10.10) or later
use scripting additions

-- code to test the Mail rule script:
tell application "Mail"
	set theSel to selection -- selected emails will be used for test
end tell
using terms from application "Mail"
	perform mail action with messages theSel in mailboxes (missing value) for rule (missing value)
end using terms from

-- the Mail rule script itself:
using terms from application "Mail"
	on perform mail action with messages messageList in mailboxes mbox for rule aRule
		repeat with msg in messageList
			set _source to source of msg
			display notification "" & subject of msg
			-- TODO: parse out the link from the source
			-- TODO: open it in safari
			-- TODO: archive the email (or delete it)
		end repeat
	end perform mail action with messages
end using terms from
