#!/bin/sh -e
# sh cheatsheet: https://github.com/dylanaraps/pure-sh-bible

# template

scriptname() {
  basename "$0"
}

description() {
    echo "${SCRIPT_DESCRIPTION}"
}

version() {
    echo "0.0.1"
}

dependencies() {
    echo "${dependencies}"
}

usage() {
    echo "NAME:"
    echo "  $(scriptname) - $(description)"
    echo ""
    echo "VERSION:"
    echo "  v$(version)"
    echo ""
    echo "USAGE:"
    echo "  $(scriptname) [-h]"
    echo ""
    echo "OPTIONS:"
    echo "  --help, -h  display help"
    exit 1
}

for flag in "$@"; do
  case "\$flag" in
    -h|--help) usage; break;;
    *) usage; break;;
  esac
done

check-dependencies "$(dependencies)" || exit 1

# begin code