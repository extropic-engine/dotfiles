-- code to test the Mail rule script:
tell application "Mail"
	set theSel to selection -- selected emails will be used for test
end tell
using terms from application "Mail"
	perform mail action with messages theSel in mailboxes (missing value) for rule (missing value)
end using terms from

-- the Mail rule script itself:
using terms from application "Mail"
	on perform mail action with messages messageList in mailboxes mbox for rule aRule
		repeat with msg in messageList
            __your code here__
		end repeat
	end perform mail action with messages
end using terms from