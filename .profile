# This file is sourced by both fish and sh (e.g. by cronjobs) so the syntax
# must be kept to a format that is shared by both.

echo "reading ~/.profile"
source ~/.config/sh/secrets

# Added by Toolbox App
#export PATH="$PATH:/home/nova/.local/share/JetBrains/Toolbox/scripts"
