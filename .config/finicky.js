// Use https://finicky-kickstart.now.sh to generate basic configuration
// Learn more about configuration options: https://github.com/johnste/finicky/wiki/Configuration

const apps = {
  Firefox: "Firefox",
  Mail: "Mail",
  Safari: "Safari",
  Slack: "Slack",
  Spark: "Spark",
  Zoom: "us.zoom.xos"
}

module.exports = {
  defaultBrowser: apps.Safari,
  options: {
    //logRequests: true,
    hideIcon: false
  },
  rewrite: [
  ],
  handlers: [
    // open mail links in the mail app
    // TODO: this does not work because finicky only handles http and https links???
    /*
    {
      browser: apps.Spark,
      match: ({ url }) => { url.protocol === "mailto" }
    },
    */
    // open slack links in the slack app
    // it seems like slack urls open a page that does some kind of rewriting to a deeplink and then opens that
    // TODO: figure out the deeplink format, maybe using ProxyMan, and the do the rewrite here instead?
    // see https://gist.github.com/NovaRemitly/063df255017236ab76563e1784b9b254
    /*
    {
      browser: apps.Slack,
      match: ({ url, urlString }) => {
        finicky.log(JSON.stringify(url))
        return false
        //return url.protocol === "https" && url.host === "remitly.slack.com" && url.pathname.startsWith("/archives/")
      }
    },
    */
    // open zoom links in the zoom app
    {
      browser: apps.Zoom,
      match: ({ url }) => {
        // make sure it's a zoom web link
        if (url.protocol !== "https" || !url.host.endsWith("zoom.us")) {
          return false;
        }
        // open non-video links in the browser
        if (url.host === "application.zoom.us" || url.host === "marketplace.zoom.us") {
          return false;
        }
        return true;
      }
    },
    // Safari can't handle this particular behavior for some reason
    {
      browser: apps.Firefox,
        match: [
          "https://remitly.okta.com/app/remitly_redshiftsamlssoprod2_1/exka1pn8emAXHarco357/sso/saml",
          "https://remitly.okta.com/app/remitly_redshiftsamlssopreprod2_1/exk4yo9xrhDMCNiiZ357/sso/saml"
        ]
    }
  ]
}
