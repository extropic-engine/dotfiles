
if not [ $MUTE ]; echo "loading git.fish" | lolcat; end

###############################################################################
# Prompt
#

set -g __fish_git_prompt_show_informative_status 1
set -g __fish_git_prompt_hide_untrackedfiles     1
set -g __fish_git_prompt_color_branch            magenta
set -g __fish_git_prompt_showupstream            "informative"
set -g __fish_git_prompt_char_upstream_ahead     "↑"
set -g __fish_git_prompt_char_upstream_behind    "↓"
set -g __fish_git_prompt_char_upstream_prefix    ""
set -g __fish_git_prompt_char_stagedstate        "+"
set -g __fish_git_prompt_char_dirtystate         "*"
set -g __fish_git_prompt_char_untrackedfiles     "%"
set -g __fish_git_prompt_char_conflictedstate    "X"
set -g __fish_git_prompt_char_cleanstate         ""
set -g __fish_git_prompt_color_dirtystate        blue
set -g __fish_git_prompt_color_stagedstate       yellow
set -g __fish_git_prompt_color_invalidstate      red
set -g __fish_git_prompt_color_untrackedfiles    $fish_color_normal
set -g __fish_git_prompt_color_cleanstate        green

###############################################################################
# Aliases
#

alias g     'git'
alias gbr   'git branch'
alias gitbr 'git branch'
alias gco   'git checkout'
alias gf    'git fetch'
alias gfu   'git fetch upstream'
alias gpl   'git pull'
alias gplu  'git pull upstream'
alias gplum 'git pull upstream master'
alias gp    'git push'
alias gpf   'git push --force'
alias gpu   'git push upstream'
alias grb   'git rebase'
alias gs    'git stash'
alias gsp   'git stash pop'
alias gstat 'git status'
alias gprl  'git prl'

###############################################################################
# Env
#

set -x GITHUB_SSH_KEY_PATH ~/.ssh
