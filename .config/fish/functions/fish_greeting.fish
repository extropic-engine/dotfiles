function fish_greeting

  if [ -z (which lolcat) ]
    alias lolcat cat
  end

  if not [ -z (which figlet) ]
    figlet -c (uname -n | sed 's/.local//') | lolcat
    echo
  end

  if not [ -z (which neofetch) ]
    neofetch --image ascii
  else if not [ -z (which screenfetch) ]
    screenfetch
  end

  # TODO: it would be cool to re-enable these if we could do them in the background / parallel
  #curl "wttr.in/Seattle?0"
  #curl "rate.sx/?n=0"

  uname -a | lolcat
  echo

  if not [ -z (which uptime) ]
    uptime | lolcat
    echo
  end

  if not [ -z (which who) ]
    echo There are (who | wc -l | xargs) users currently active.\n | lolcat
  end

  echo Welcome back, (whoami).\n | lolcat

  if not [ -z (which fortune) ]
    fortune | lolcat
    echo
  end

end
