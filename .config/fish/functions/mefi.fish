function mefi --description 'SSH into mefi with automatic reconnects'
  if [ "$TERM" = 'st-256color' ]
    set TERM 'xterm-256color'
  end
  autossh -M 2000 mefi
end
