function checkargs --description "Ensures that the number of args provided matches what is expected. Use like: `checkargs $argv 2; or return -1`"
  if [ (count $argv) -lt 1 ]
    echo -e "fish function 'checkargs' must have at least 1 argument.\n" (status print-stack-trace)
    return -1
  end

  set arg_count (math (count $argv) " - 1")

  if [ $arg_count -lt $argv[-1] ]
    echo -e "fish function expected "$argv[-1]" args, got "$arg_count".\n" (status print-stack-trace)
    return -1
  end
end
