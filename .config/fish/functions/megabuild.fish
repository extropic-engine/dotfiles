function megabuild --description 'Figure out how to build the project in the current directory. The first argument should be the name of the project.'
  set projectName (basename (pwd))
  event 'build-started' $projectName
  if [ -e pom.xml ]
    mvn package -DskipTests; set buildStatus $status
  else if [ -e gradlew ]
    ./gradlew clean assemble; set buildStatus $status
  else if [ -e deployment/build_forge ]
    deployment/build_forge --skipTests; set buildStatus $status
  else if [ -e main.go ]
    go build; set buildStatus $status
  else if [ -e bin/build.sh ]
    ./bin/build.sh; set buildStatus $status
  else
    echo "Can't figure out how to build $projectName."; set buildStatus -1
  end
  event 'build-finished' $projectName $buildStatus
  return $buildStatus
end
