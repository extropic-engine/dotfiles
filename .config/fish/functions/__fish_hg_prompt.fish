
# Taken from https://gist.github.com/CrshOverride/9840450

function __fish_hg_prompt --description 'Promp function for Mercurial'
  # Colors
  set -l color_branch         (set_color magenta)
  set -l color_branch_behind  (set_color red)
  set -l color_cleanslate     (set_color green)
  set -l color_added          (set_color green)
  set -l color_modified       (set_color blue)
  set -l color_removed        (set_color red)
  set -l color_unknown        (set_color magenta)
  set -l color_deleted        (set_color cyan)
  set -l color_renamed        (set_color yellow)
  set -l color_tag            (set_color 808080)
  set -l color_normal         (set_color normal)

  # Info
  set -l commit        ''
  set -l tags          ''
  set -l modifications ''
  set -l additions     ''
  set -l removals      ''
  set -l deletions     ''
  set -l unknowns      ''
  set -l renames       ''
  set -l repo_summary  (command hg summary ^/dev/null)
  set -l is_clean      1
  set -l is_behind     1
  set -l hg_prompt     ''

  # If (hg summary) failed, we're not in an hg repo so return
  test -n "$repo_summary"; or return

  for line in $repo_summary
    switch $line
      case 'parent*'
        for parent_line in (echo $line | sed 's/parent: //g' | tr ' ' '\n')
          if test -n "$commit"
            set tags $tags $parent_line
          else
            set commit $parent_line
          end
        end
      case 'update*'
        set -l update_value (echo $line | sed 's/update: //g')
        if test "$update_value" != "(current)"
          set is_behind 0
        end
      case 'branch*'
        set -g __fish_hg_prompt_branch (echo $line | sed 's/branch: //g')
      case 'commit*'
        for change_line in (echo $line | sed 's/commit: //g' | tr ',' '\n' | tr -d ' ')
          switch $change_line
            case '(clean)'
              set is_clean 0
            case '*modified'
              set modifications (echo $change_line | sed 's/modified//g')
            case '*added'
              set additions (echo $change_line | sed 's/added//g')
            case '*removed'
              set removals (echo $change_line | sed 's/removed//g')
            case '*deleted'
              set deletions (echo $change_line | sed 's/deleted//g')
            case '*unknown'
              set unknowns (echo $change_line | sed 's/unknown//g')
            case '*renamed'
              set renames (echo $change_line | sed 's/renamed//g')
          end
        end
    end
  end

  set -l hg_prompt "$color_normal ("

  if test -n "$__fish_hg_prompt_branch"
    if test $is_behind -eq 0
      set hg_prompt "$hg_prompt$color_branch_behind$__fish_hg_prompt_branch$color_normal |"
    else
      set hg_prompt "$hg_prompt$color_branch$__fish_hg_prompt_branch$color_normal |"
    end
  end

  if test $is_clean -eq 0
    set hg_prompt "$hg_prompt$color_cleanslate $__fish_git_prompt_char_cleanstate"
  end

  if test -n "$additions"
    set hg_prompt "$hg_prompt$color_added +$additions"
  end

  if test -n "$modifications"
    set hg_prompt "$hg_prompt$color_modified ~$modifications"
  end

  if test -n "$removals"
    set hg_prompt "$hg_prompt$color_removed -$removals"
  end

  if test -n "$unknowns"
    set hg_prompt "$hg_prompt$color_unknown ?$unknowns"
  end

  if test -n "$deletions"
    set hg_prompt "$hg_prompt$color_deleted !$deletions"
  end

  if test -n "$renames"
    set hg_prompt "$hg_prompt$color_renamed -$renames"
  end

  if test -n "$tags"
    set hg_prompt "$hg_prompt$color_normal | $color_tag" (echo $tags | sed -e 's/^[ ]*//g')
  end

  set -l hg_prompt "$hg_prompt$color_normal)"

  echo $hg_prompt
end
