function get-all-remitly-error-logs --description 'Output all error log paths used within Remitly'
  # PHP error logs across all PHP services
  echo "/var/log/apache2/error.log"
  # PHP error logs for a specific service
  for service in (get-remitly-apache-services)
    set service (echo "$service" | tr '[:upper:]' '[:lower:]')
    echo "/var/log/apache2/"$service"_error.log"
  end
  for service in (get-remitly-services)
    set service (echo "$service" | tr '[:upper:]' '[:lower:]')
    # Location for Forge services
    echo "/var/deploy/logs/$service/current"
    # Location for Forge services in the beta environment
    echo "/var/log/$service/current"
  end
end
