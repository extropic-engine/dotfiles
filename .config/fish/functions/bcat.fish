# TODO: this does not work right with files that have spaces in the name
function cat
  if [ (count $argv) -eq 1 ]
    if [ -d "$argv[1]" ]
      ls $argv[1]
      return
    else
      if not [ -z (which sxiv) ]
        switch "$argv[1]"
          case '*.png' '*.jpg' '*.gif' '*.jpeg' '*.bmp'
            sxiv $argv[1]
        end
      end
    end
  end
  if not [ -z (which bat) ]
    bat $argv
  else
    eval (which cat) $argv
  end
end

