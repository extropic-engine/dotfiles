function jrnl
  pushd "$EXOCORTEX/ivy/journal"
  and git pull
  and echo "##### "(date) >> (date "+%Y-%m-%d.md")
  and vim (date "+%Y-%m-%d.md")
  and git add *.md
  and git commit -m "jrnl "(date "+%Y-%m-%d.md")
  and git push
  and popd
end

