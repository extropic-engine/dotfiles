function fish_prompt --description 'Write out the prompt'

printf '[%s] ' (date +"%H:%M:%S")

set -l last_status $status

if not set -q __fish_prompt_normal
	set -g __fish_prompt_normal (set_color normal)
end

###############################
# PWD
#

set_color $fish_color_cwd
echo -n (prompt_pwd)
set_color normal

################################
# SMS notes sync background task
#

#if [ -e "$EXOCORTEX/import_sms_notes.sh" ]
#
#  if not [ -e "/tmp/last_sms_notes_sync" ]
#    echo (date +%s) > /tmp/last_sms_notes_sync
#  end
#
#  set LAST_SYNC (echo (date +%s) - (stat --format '%m' /tmp/last_sms_notes_sync) | bc)
#  if [ "$LAST_SYNC" -gt "3600" ]
#    eval $EXOCORTEX/import_sms_notes.sh 1> /usr/local/var/log/sms_sync 2> /usr/local/var/log/sms_sync &
#    echo -n " <~ synchronizing ~>"
#    touch /tmp/last_sms_notes_sync
#  end
#end


#######################
# Mercurial statusline
#
#

if [ -z (which hg) ]
	printf '%s ' (__fish_git_prompt)
else
	printf '%s ' (__fish_git_prompt) (__fish_hg_prompt)
end

	if not test $last_status -eq 0
	set_color $fish_color_error
	end

	echo -n '$ '

end
