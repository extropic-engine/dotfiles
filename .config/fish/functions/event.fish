function event --description "Hook to trigger various actions based on events"
  checkargs $argv 1; or return -1

  switch "$argv[1]"
    case 'build-started'
      #tmux set-option -w window-status-style bg=yellow,fg=black
      #tmux rename-window $argv[2]" (Building)"
    case 'build-success'
      checkargs $argv 2; or return -1
      noti -m $argv[2]" build succeeded!" &
      #tmux set-option -w window-status-style bg=green,fg=black
      #tmux rename-window $argv[2]" (OK)"
    case 'build-failed'
      checkargs $argv 2; or return -1
      noti -m $argv[2]" build failed!" &
      #tmux set-option -w window-status-style bg=red,fg=black
      #tmux rename-window $argv[2]" (Failed)"
    case 'build-finished'
      checkargs $argv 3; or return -1;
      if [ $argv[3] = 0 ]
        event 'build-success' $argv[2]
      else
        event 'build-failure' $argv[2]
      end
    case '*'
      # do nothing
  end
end

