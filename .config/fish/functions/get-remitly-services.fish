# The service name should match the name of the Github repo, and also
# the Forge app name (when lowercased.)
# TODO: figure out a workaround when the app name does not match the repo name.
function get-remitly-services --description 'List all Remitly services.'
  echo "ClientService"
  echo "CXCoreService"
  echo "DisbursementService"
  echo "FundingService"
  echo "RiskService"
  echo "RemittanceCatalog"
  echo "PricingService"
  echo "PromotionService"
  echo "narwhal"
end

