function get-available-error-logs --description 'Determine which error logs are available from the current environment'
  for log in (get-all-remitly-error-logs)
    if [ -e $log ]
      echo "$log"
    end
  end
end

