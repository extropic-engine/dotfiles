function get-all-remitly-access-logs --description 'Output all access log paths used within Remitly'
  # PHP error logs across all PHP services
  echo "/var/log/apache2/access.log"
  # PHP error logs for a specific service
  for service in (get-remitly-apache-services)
    echo "/var/log/apache2/"$service"_access.log"
  end
  for service in (get-remitly-dropwizard-services)
    # Location for JVM services in Forge
    echo "/var/deploy/logs/$service/current"
    # Location for JVM services in the beta environment
    echo "/var/log/$service/current"
  end
end

