function megaup --description 'Bring up all available Forge services locally.'
  # TODO: a progress bar would be sick
  for service in (get-remitly-services)
    pushd $REMITLY_HOME/$service
    vagrant up; and git pull; and megabuild; and forge pkg; and forge provision
    # TODO: run noti?
    popd
  end
end
