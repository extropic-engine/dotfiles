if not [ $MUTE ]; echo "Loading paths.fish" | lolcat; end

if [ -d "$HOME/go" ]
  set -x GOPATH $HOME/go
end

set bin_directories \
    $GOPATH/bin \
    /usr/local/go/bin \
    $HOME/.composer/vendor/bin \
    $HOME/.cargo/bin \
    $HOME/bin \
    $HOME/.bin \
    $HOME/.local/bin

for dir in $bin_directories
    if [ -d "$dir" ]
        set -x PATH $PATH $dir
    end
end

# Set up XDG paths
if [ -z "$XDG_DATA_HOME" ]
  set -x XDG_DATA_HOME   $HOME/.local/share
end
if [ -z "$XDG_CONFIG_HOME" ]
  set -x XDG_CONFIG_HOME $HOME/.config
end
if [ -z "$XDG_DATA_DIRS" ]
  set -x XDG_DATA_DIRS   /usr/local/share/:/usr/share/
end
if [ -z "$XDG_CACHE_HOME" ]
  set -x XDG_CACHE_HOME  $HOME/.cache
end
