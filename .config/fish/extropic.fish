###############################################################################
#
# Configurations that should only apply to my personal computers, not servers or VMs
#

if not [ $MUTE ]; echo "Loading extropic.fish" | lolcat; end

# hell yes

abbr -a weather 'curl "wttr.in/Seattle"'
abbr -a wtr 'curl "wttr.in/Seattle"'

abbr -a btc 'curl rate.sx/btc'
abbr -a eth 'curl rate.sx/eth'
abbr -a xmr 'curl rate.sx/xmr'

abbr -a pb 'pinboard'


abbr -a copyinit 'echo "sh -c \"\$(curl -fsSL https://gitlab.com/extropic-engine/dotfiles/raw/master/.bin/initialize.sh)\"" | pbcopy'

abbr -a exup 'pushd $EXOCORTEX; and git add .; and git commit -m "exup"; and git push; popd'

# Path variables and aliases

set -x EXTROPIC_HOME    $HOME/code/extropic
set -x EXOCORTEX        $EXTROPIC_HOME/exocortex
set -x DOSSIERS         $EXTROPIC_HOME/dossiers
set -x EXTROPIC_STUDIOS $EXTROPIC_HOME/ExtropicStudios
set -x TACHIKOMA        $EXTROPIC_HOME/tachikoma
set -x TRILITHON        $GOPATH/src/gitlab.com/extropic-engine/trilithon

# disable the garbo

set -x DOTNET_CLI_TELEMETRY_OPTOUT 1

abbr -a exo  'cd $EXOCORTEX'
abbr -a es   'cd $EXTROPIC_STUDIOS'
abbr -a dos  'cd $DOSSIERS'
abbr -a tch  'cd $TACHIKOMA'
abbr -a lith 'cd $TRILITHON'
