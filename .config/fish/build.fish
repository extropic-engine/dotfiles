
if not [ $MUTE ]; echo "Loading build.fish" | lolcat; end

function map-to-pretty-name --description 'Maps project names to their canonical values for user-readability'
  switch $argv[1]
  case 'devdb'; echo 'Development Database'
  default; echo $argv[1]
  end
end

abbr -a b 'megabuild'

function runTests --description 'Test it!'
  set projectName (basename (pwd))" Tests"
  event-build-started $projectName
  if [ -e pom.xml ]
    mvn test; set buildStatus $status
  else if [ -e gradlew ]
    ./gradlew test; set buildStatus $status
  else
    echo "Can't figure out how to test $cwd."; set buildStatus -1
  end
  event-build-finished $projectName $buildStatus
  return $buildStatus
end

abbr -a bt 'b; and runTests'
