
source ~/.profile

# alias lolcat if it's not installed
if [ -z (which lolcat) ]
  alias lolcat 'cat'
end

# don't make little noises from vim
if not [ -z "$VIMRUNTIME" ]; set MUTE 1; end

if not [ $MUTE ]
  echo "Loading config.fish ("(whoami)"@"(uname -n)")" | lolcat
end

source ~/.config/fish/paths.fish

###############################################################################
# Environment
#

set -x EDITOR 'vim'

# Configure GPG nonsense
set -x GPG_TTY (tty)

###############################################################################
# Aliases
#

# TODO: only define aliases if the relevant tool is installed

# Aliases
abbr -a u      'cd ..'
abbr -a uu     'cd ../..'
abbr -a uuu    'cd ../../..'
abbr -a uuuu   'cd ../../../..'
abbr -a uuuuu  'cd ../../../../..'
abbr -a uuuuuu 'cd ../../../../../..'
abbr -a less   'less -R'
abbr -a up     'uptime'
abbr -a cl     'clear'

# libreboot aliases
abbr -a crypt   'gpg2 -ea -r josh@extropicstudios.com'
abbr -a uncrypt 'gpg2 -d'
abbr -a sabcrypt 'gpg2 -ea -r 217AF77BB7980AAA35E6EA616856D8AFD3CF6739'
abbr -a v 'vim -u ~/.vimrc.minimalist'

# exa aliases
if not [ -z (which exa) ]
  abbr -a ls  'exa'
  abbr -a lls 'ls -hl@ --git'
  abbr -a lst 'ls -T -L 10'
end

if not [ -z (which colordiff) ]
  abbr -a diff 'colordiff'
end

if not [ -z (which hledger) ]
  abbr -a hl 'hledger'
end

if not [ -z (which xclip) ]
  abbr -a clip 'xclip -selection clipboard'
  abbr -a put 'xclip -selection clipboard -o'
end

if not [ -z (which docker-machine) ]
  abbr -a ds 'docker-machine start default; and eval (docker-machine env default)'
end

# TaskWarrior aliases
abbr -a t   'task'
abbr -a tl  'task list'
abbr -a ta  'task add'
abbr -a taw 'task add +WORK'
abbr -a tm  'task modify'
abbr -a td  'task done'
abbr -a ts  'task sync'
abbr -a tsm 'task summary'
abbr -a tbd 'task burndown.daily'
abbr -a ti  'task info'
abbr -a tn  'task next'
abbr -a tce 'task context extropic'
abbr -a tcw 'task context work'
abbr -a tcg 'task context game'
abbr -a tan 'task annotate'

# Useful commands
abbr -a urlencode 'python -c "import sys, urllib as ul; print ul.quote_plus(sys.argv[1])"'
abbr -a urldecode 'python -c "import sys, urllib as ul; print ul.unquote_plus(sys.argv[1])"'
abbr -a passgen 'date +%s | shasum -a 256 | base64 | head -c 32; echo'

###############################################################################
# Linked Configs
#

# TODO: what if this just ls'ed all the configs in this directory and then each config had its own detection for whether it should be loaded
# TODO: move non-fish-specific configs to posix shell
# TODO: add timing to report how long each config file takes to load

# General configs

source ~/.config/fish/extropic.fish
source ~/.config/fish/git.fish
source ~/.config/fish/build.fish
source ~/.config/fish/gaming.fish

# OS-specific configs

if [ (uname) = 'Darwin' ]
  source ~/.config/fish/macos.fish
  source ~/.config/fish/iterm2_shell_integration.fish
end

if not [ -z (uname -v | grep 'trisquel\|ubuntu') ]
  source ~/.config/fish/ubuntu.fish
end
