###############################################################################
#
# Configurations that relate to gaming, gamer girls, monster energy drinks, transgender, etc
#

if not [ $MUTE ]; echo "Loading $0" | lolcat; end

# Path variables and aliases

# these paths are different by os, should be doing some detection here
if [ (uname) = 'Darwin' ]
  set -x RIMWORLD_USERDATA $HOME/Library/Application\ Support/RimWorld
  set -x RIMWORLD_GAMEDATA $HOME/Library/Application\ Support/Steam/steamapps/common/RimWorld/RimWorldMac.app
  set -x FACTORIO_USERDATA $HOME/Library/Application\ Support/factorio
else
  set -x RIMWORLD_USERDATA $HOME/.config/unity3d/Ludeon\ Studios/RimWorld\ by\ Ludeon\ Studios
  set -x RIMWORLD_GAMEDATA $HOME/.steam/debian-installation/steamapps/common/RimWorld
  set -x RIMWORLD_WORKSHOPDATA $HOME/.steam/debian-installation/steamapps/workshop/content/294100
  set -x FACTORIO_USERDATA $HOME/.factorio
end

abbr -a gorwud 'cd $RIMWORLD_USERDATA'
abbr -a gorwgd 'cd $RIMWORLD_GAMEDATA'
abbr -a gorwwd 'cd $RIMWORLD_WORKSHOPDATA'
abbr -a gofud 'cd $FACTORIO_USERDATA'
