if not [ $MUTE ]; echo "Loading remitly.fish" | lolcat; end

###############################################################################
# Env Variables
#

# These are used by opctl
set -x github '{"username": "NovaRemitly", "accessToken": "'$GITHUB_ACCESS_TOKEN'"}'

set -x JAVA_HOME (/usr/libexec/java_home -v 1.8)

###############################################################################
# Aliases and Functions
#

# Building & Testing

abbr -a bump        'sudo sv restart discoserv avahi-daemon'
abbr -a bunp        'sudo sv restart discoserv avahi-daemon'

abbr -a bp 'megabuild; and forge pkg; noti -m (basename (pwd))" build finished!" &'

abbr -a mpurge 'mvn dependency:purge-local-repository -DactTransitively=false -DreResolve=false'

###############################################################################
#
# Log Management
#

function lt --description 'Tail all available error logs.'
  set LOGS (get-available-error-logs)
  echo "tailing $LOGS" | lolcat
  eval tail -f --follow=name --retry $LOGS
end

function lta --description 'Tail all available access and error logs.'
  set LOGS (string join \n (get-all-remitly-error-logs) (get-all-remitly-access-logs) | sort | uniq)
  echo "tailing $LOGS" | lolcat
  eval tail -f --follow=name --retry $LOGS
end

function runtests --description 'Finds platform unit tests and then runs them'
  pushd /var/deploy/apps/platformphp/latest/src/app/tests
  ./phpunit-5.7.phar $argv
  popd
end
