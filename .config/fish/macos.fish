
if not [ $MUTE ]; echo "Loading macos.fish" | lolcat; end

#########
# Paths #
#########

if [ -e "/usr/local/bin/brew" ]
  eval (/usr/local/bin/brew shellenv)
else if [ -e "/opt/homebrew/bin/brew" ]
  eval (/opt/homebrew/bin/brew shellenv)
else
  echo "  Homebrew missing"
end

if [ -d "$HOMEBREW_PREFIX/opt/ruby/bin" ]
  set PATH $HOMEBREW_PREFIX/opt/ruby/bin $PATH
end

if [ -d "$HOMEBREW_PREFIX/opt/node@8/bin" ]
  set -g fish_user_paths "$HOMEBREW_PREFIX/opt/node@8/bin" $fish_user_paths
end

if [ -d "$HOMEBREW_PREFIX/opt/go/libexec/bin" ]
  set PATH $HOMEBREW_PREFIX/opt/go/libexec/bin $PATH
end

if [ -d "$HOMEBREW_PREFIX/opt/gnu-tar/libexec/gnubin" ]
  set PATH $HOMEBREW_PREFIX/opt/gnu-tar/libexec/gnubin $PATH
end

###############
# Environment #
###############

# Set 256 color mode
set -x TERM xterm-256color
set -x PAGER 'less -R'

# Noti config
set -x NOTI_DEFAULT 'banner speech'
set -x NOTI_SAY_VOICE 'Ava'

# Opt out of Homebrew analytics tracking. Jerks
set -x HOMEBREW_NO_ANALYTICS 1

###########
# Aliases #
###########

# elinks on OS X won't load https pages for some reason
alias lynx   'links'
alias elinks 'links'
alias uffda  'brew update; and brew upgrade; and brew cleanup'
# BSD less prints colors as escape sequences, -R disables that
alias less   'less -R'
alias fixdns 'sudo killall -HUP mDNSResponder'
alias clip   'pbcopy'
alias finder-show 'defaults write com.apple.finder AppleShowAllFiles TRUE; and killall Finder'
alias finder-hide 'defaults write com.apple.finder AppleShowAllFiles FALSE; and killall Finder'
abbr -a edit-url-scheme-defaults 'open -a Xcode ~/Library/Preferences/com.apple.LaunchServices/com.apple.launchservices.secure.plist'

alias ungatekeep 'xattr -dr com.apple.quarantine'

##########
# Sources

# iterm2 configs
test -e {$HOME}/.iterm2_shell_integration.fish ; and source {$HOME}/.iterm2_shell_integration.fish
