
if not [ $MUTE ]; echo "Loading ubuntu.fish" | lolcat; end

alias bat   'upower -i /org/freedesktop/UPower/devices/battery_BAT0| grep -E "state|to\ full|percentage"'
alias uffda 'sudo sh -c "apt-get update && apt-get upgrade"'

abbr -a sshot 'xwd -root -display :0 | convert - png:- > (date '+Screenshot-%-e-%b-%Y-%-k-%M.png')'

