#!/bin/sh -e

# TODO: documentation

# Run this script to create a new Mefi instance

echo "Your SSH key is $(ssh-add -l -E md5)"
SSH_SOMBERBLOCK="85:08:ee:38:58:88:49:f9:bd:58:63:96:3d:2b:98:80"
SSH_LIBREBOOT="f6:77:34:4b:6b:1f:9a:3c:ea:45:e7:4b:7a:80:86:be"
SSH_BLACKPHONE="3f:e7:97:37:2e:d7:0a:9c:02:6c:f1:9e:31:c7:1b:bb"
echo "Using $SSH_SOMBERBLOCK to provision host."
echo "Using $SSH_LIBREBOOT to provision host."
echo "Using $SSH_BLACKPHONE to provision host."

# SSH key is somber-block
doctl compute droplet create \
  mefi \
  --enable-ipv6 \
  --size 1gb \
  --image freebsd-11-0-x64-zfs \
  --region sfo2 \
  --ssh-keys "$SSH_SOMBERBLOCK" \
  --ssh-keys "$SSH_LIBREBOOT" \
  --ssh-keys "$SSH_BLACKPHONE"
  # not currently supported with freebsd?
  # --enable-backups \

echo "WARNING: Digital Ocean does not yet support creating FreeBSD 11 images"
echo "with backups enabled. You need to manually enable backups after instance"
echo "creation."
echo
echo "Now connect to your instance by running"
echo "  doctl compute ssh --ssh-user freebsd <droplet_id>"
echo "and follow the checklist:"
echo "https://gitlab.com/extropic-engine/checklists/blob/master/provision_freebsd.md"
