A place to put scripts that are not yet capable of performing their primary
function, to keep them out of `$PATH`.