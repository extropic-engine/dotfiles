#!/bin/bash -eu

# TODO: documentation

## "Safe" dd wrapper
## by Eigenellies

# This script attempts to make it slightly safer to run "dd" to write to block devices by first
# checking if the block device to be written is mounted, and refusing to continue if so.
# This can, for example, prevent you from accidentally overwriting your root filesystem when you
# meant to write to a USB flash drive.

# This script is NOT GUARANTEED or EVEN IMPLIED TO BE USEFUL in keeping your system safe from
# accidental overwrites, or from any other error. NO LIABILITY is assumed by the script's authors 
# for any consequences of running this script, to the full extent permitted by applicable law.

# This file and all of its contents are hereby released into the public domain.

if [[ -z $1 ]] || [[ -z $2 ]]; then
	echo "you need to specify an input file and an output file, please."
	exit 1
fi

if ! [[ -e $1 ]]; then
	echo "input $1 does not exist."
fi

# if you want this script to be able to create new files, remove or comment out the following three lines:
if ! [[ -e $2 ]]; then
	echo "output $2 does not exist."
fi

# if getting the list of mounted devices fails, we SHOULD NOT assume that everything is fine.
mounts=`mount`
if [[ -z $mounts ]]; then
	echo "can't figure out what's mounted and what's not. you'll have to do it yourself, sorry."
	exit 1
fi

# search each line of the output of `mount` and check to see if the device we're supposed to write is there.
# since this uses a regex match with .*, it will catch cases where, say, /dev/sda is the specified output and
# /dev/sda1 is mounted. since most systems use partitions, this is important.
found_dev=0
while read -r line; do
	if [[ $line =~ ^$2.* ]]; then
		echo "no. this device is mounted. slow down and try again."
		exit 1
	fi
	if [[ $line =~ ^/dev/.* ]]; then
		found_dev=1
	fi
done <<< $mounts

# basic sanity check of the "mount" command output: if it doesn't have any /dev entries, assume it's not trustworthy.
# if for some reason your system actually doesn't have any mounted block devices in /dev, and that's normal. then you
# can remove or comment the following five lines to bypass this check.
if [ $found_dev = 0 ]; then
	echo "uh oh. it looks like the output of the 'mount' command didn't contain any entries in /dev... that's weird."
	echo "something seems fishy about this, so we're not going to continue."
	exit 1
fi

# if we have made it down here without exiting, then we know of no reason not to continue with the operation.
dd if=$1 of=$2 bs=4M oflag=sync status=progress
